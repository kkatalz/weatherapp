import { useCookies } from "react-cookie";

const AUTH_KEY = "auth-key";

const useAuth = () => {
  // custom hook
  const [cookies, setCookie, removeCookie] = useCookies([AUTH_KEY]);
  const authKey = cookies["auth-key"] || null;

  const logIn = (key) => {
    setCookie(AUTH_KEY, key);
  };

  const logOut = () => {
    removeCookie(AUTH_KEY, { path: "/" });
  };

  return { authKey, logIn, logOut };
};

export default useAuth;
