import { Icon } from "@chakra-ui/react";
import { WiDaySunny, WiCloudy, WiRain, WiSnow } from "react-icons/wi";

const getWeatherIcon = (conditions) => {
  if (conditions.includes("rain")) {
    return <Icon as={WiRain} boxSize={8} />;
  } else if (conditions.includes("snow")) {
    return <Icon as={WiSnow} boxSize={8} />;
  } else if (conditions.includes("cloud")) {
    return <Icon as={WiCloudy} boxSize={8} />;
  } else {
    return <Icon as={WiDaySunny} boxSize={8} />;
  }
};

const getWeatherInfo = (weather, dayIndex) => {
  const startIndex = dayIndex * 8;
  const dayTemperature = weather.hourly.temperature_2m.slice(
    startIndex,
    startIndex + 8
  );
  const dayHumidity = weather.hourly.relative_humidity_2m.slice(
    startIndex,
    startIndex + 8
  );
  const dayWindSpeed = weather.hourly.wind_speed_10m.slice(
    startIndex,
    startIndex + 8
  );

  const minTemperature = Math.min(...dayTemperature);
  const maxTemperature = Math.max(...dayTemperature);
  const avgHumidity =
    dayHumidity.reduce((acc, val) => acc + val, 0) / dayHumidity.length;
  const avgWindSpeed =
    dayWindSpeed.reduce((acc, val) => acc + val, 0) / dayWindSpeed.length;

  return {
    minTemperature,
    maxTemperature,
    avgHumidity,
    avgWindSpeed,
  };
};

export { getWeatherIcon, getWeatherInfo };
