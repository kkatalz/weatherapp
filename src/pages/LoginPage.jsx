import React, { useState } from "react";
import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button,
  Heading,
  Stack,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { SECRET } from "../secret";
import useAuth from "../auth";

const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);

  const { logIn } = useAuth(); // custom hook
  const navigate = useNavigate(); // hook

  const isWrongEmail = !email.startsWith(SECRET);
  const isWrongPassword = password !== SECRET;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isWrongEmail || isWrongPassword) setIsError(true);
    else {
      setIsError(false);
      const key = email;
      logIn(key);

      navigate("/dashboard");
    }
  };

  return (
    <div className="login-page">
      <form className="registration-form" onSubmit={handleSubmit}>
        <Stack alignItems={"center"}>
          <Heading>Log in to see weather</Heading>
        </Stack>
        <FormControl isInvalid={isError}>
          <FormLabel marginTop={"15px"}>Email address</FormLabel>
          <Input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          {isWrongEmail && <FormErrorMessage>Email is wrong.</FormErrorMessage>}
          {/* password */}
          <FormLabel marginTop={"10px"}>Password</FormLabel>
          <Input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          {isWrongPassword && (
            <FormErrorMessage>Password is wrong.</FormErrorMessage>
          )}
        </FormControl>

        <Stack alignItems={"center"}>
          <Button marginTop={"10px"} type="submit">
            Login
          </Button>
        </Stack>
      </form>
    </div>
  );
};

export default LoginPage;
