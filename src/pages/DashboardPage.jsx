import { Flex, Spinner } from "@chakra-ui/react";
import { format, isToday } from "date-fns";
import React from "react";
import { Navigate } from "react-router-dom";
import useAuth from "../auth";
import NavBar from "../components/NavBar";
import WeatherCard from "../components/WeatherCard";
import useGetWeather from "../hooks/useGetWeather";
import { getWeatherIcon, getWeatherInfo } from "../utils/weather";
import Loader from "../components/Loader";

const DashboardPage = () => {
  const { authKey } = useAuth();
  const { isLoading, weather } = useGetWeather();

  if (!authKey) return <Navigate to={"/"} />;

  return (
    <div>
      <NavBar />
      {isLoading ? (
        <Loader />
      ) : (
        <Flex
          maxWidth="1500px"
          bg="white"
          p="20px"
          borderRadius="md"
          boxShadow="md"
          marginInline="auto"
          overflowX="auto"
          marginTop="12%"
        >
          {weather.hourly?.time.slice(0, 114).map((time, index) => {
            const currentTime = new Date(time);
            const formattedTime = isToday(currentTime)
              ? format(currentTime, "h:mm a", { awareOfUnicodeTokens: true })
              : format(currentTime, "EEEE, MMM d h:mm a", {
                  awareOfUnicodeTokens: true,
                });

            if (index % 8 === 0) {
              const {
                minTemperature,
                maxTemperature,
                avgHumidity,
                avgWindSpeed,
              } = getWeatherInfo(weather, index / 8);
              const weatherConditions = "rain";
              const weatherIcon = getWeatherIcon(weatherConditions);

              const blockColors = [
                "#FF6384",
                "#36A2EB",
                "#FFCE56",
                "cyan",
                "coral",
              ]; // Example colors for every three blocks

              return (
                <WeatherCard
                  borderColor={
                    blockColors[Math.floor(index / 3) % blockColors.length]
                  }
                  key={index}
                  windSpeed={avgWindSpeed.toFixed(2)}
                  humidity={Math.round(avgHumidity)}
                  time={formattedTime}
                  weatherIcon={weatherIcon}
                  minTemperature={minTemperature}
                  maxTemperature={maxTemperature}
                />
              );
            }
          })}
        </Flex>
      )}
    </div>
  );
};

export default DashboardPage;
