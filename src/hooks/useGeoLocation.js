import { useEffect, useState } from "react";

const useGeoLocation = () => {
  const [coords, setCoords] = useState({ latitude: "", longitude: "" });

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      setCoords(position.coords);
    });
  }, []);

  return coords;
};
export default useGeoLocation;
