import { useState, useEffect } from "react";
import useGeoLocation from "./useGeoLocation";

const useGetWeather = () => {
  const { longitude, latitude } = useGeoLocation();

  const [weather, setWeather] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchWeather = async () => {
      setIsLoading(true);
      if (!latitude || !longitude) return;

      const response = await fetch(
        `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`
      );

      const weatherData = await response.json();
      if (weatherData) {
        setWeather(weatherData);
        setIsLoading(false);
      }
    };

    fetchWeather();
  }, [latitude, longitude]);

  return { weather, isLoading };
};
export default useGetWeather;
