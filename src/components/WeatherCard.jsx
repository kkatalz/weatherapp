import { Box, Text } from "@chakra-ui/react";
import React from "react";

const WeatherCard = ({
  borderColor,
  time,
  weatherIcon,
  minTemperature,
  maxTemperature,
  humidity,
  windSpeed,
}) => {
  return (
    <Box
      p="6"
      borderRadius="md"
      minWidth="350px"
      marginRight="4"
      textAlign="center"
      fontFamily="Roboto"
      fontSize="20px"
      color="white"
      border="3px solid"
      borderColor={borderColor}
      boxShadow="lg"
    >
      <Text fontWeight="bold" fontSize="22px" color="black">
        {time}
      </Text>
      {weatherIcon && (
        <Box mt="2" fontSize="40px" color="black">
          {weatherIcon}
        </Box>
      )}
      <Text fontSize="18px" color="black">
        {minTemperature}°C / {maxTemperature}°C
      </Text>
      <Text fontSize="18px" color="black">
        Humidity: {humidity}%
      </Text>
      <Text fontSize="18px" color="black">
        Wind Speed: {windSpeed} m/s
      </Text>
    </Box>
  );
};

export default WeatherCard;
