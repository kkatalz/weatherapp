import React from "react";
import { Flex, Box, Heading, Button, Stack, Select } from "@chakra-ui/react";
import useAuth, { logOut } from "../auth";
import { MdArrowDropDown } from "react-icons/md";
import { useNavigate } from "react-router-dom";

const NavBar = () => {
  const navigate = useNavigate(); // hook (always on top level)
  const { logOut } = useAuth();

  const handleLogout = () => {
    navigate("/");
    logOut();
  };

  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      padding="25px"
      bg="blue.200"
      color="white"
    >
      {/* Title on the left */}
      <Stack flexDirection={"row"} gap={"40px"} alignItems={"center"}>
        <Box>
          <Heading as="h1" fontSize="30px">
            Weather App
          </Heading>
        </Box>
        <Stack flexDirection={"row"} gap={"20px"} marginTop={"5px"}>
          <Heading as="h1" fontSize="20px">
            Home
          </Heading>
          <Select
            fontSize="20px"
            fontWeight={"bold"}
            marginTop="-2px"
            marginRight={-3}
            icon={<MdArrowDropDown />}
            variant="unstyled"
            placeholder="Language"
          />
          <Heading as="h1" fontSize="20px">
            Map
          </Heading>
          <Heading as="h1" fontSize="20px">
            API
          </Heading>
          <Heading as="h1" fontSize="20px">
            About
          </Heading>
        </Stack>
      </Stack>

      {/* Log out button on the right */}
      <Box>
        <Button
          colorScheme="whiteAlpha"
          onClick={handleLogout}
          fontSize={"20px"}
        >
          Log Out
        </Button>
      </Box>
    </Flex>
  );
};

export default NavBar;
